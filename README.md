# E-Voting-System


## Requirements
The following packages are required on Debian or on Ubuntu:
cmake git libboost-iostreams-dev libboost-atomic-dev libboost-thread-dev libboost-system-dev libboost-date-time-dev libboost-regex-dev libboost-filesystem-dev libboost-random-dev libboost-chrono-dev libboost-serialization-dev libwebsocketpp-dev openssl libssl-dev ninja-build libcpprest-dev libfftw3-dev build-essential gcc-9 g++-9 cpp-9 libtbb-dev libsqlite3-dev liblog4cplus-dev

The coordinator requirements are Java and Maven.

## Installation
Clone the Repository with all git submodules

```
cd CPP
mkdir build
cd build
cmake .. -DENABLE_TESTS=off -DENABLE_FFTW=on -DENABLE_NAYUKI_PORTABLE=off -DENABLE_NAYUKI_AVX=off -DENABLE_SPQLIOS_AVX=off -DENABLE_SPQLIOS_FMA=off -DBENCHMARK_ENABLE_TESTING=off -DGELFCPP_BUILD_TESTS=off
cmake --build .
```

It is also possible to get it to work in WSL on Windows with the exception of tbb. All the usages of tbb need to be commented, which removes optimizations, which run in parallel.

Installation for Java

```
cd JAVA\coordinator
mvn install
```

Alternatively Docker images can be downloaded from our build server:
Coordinator: https://jenkins.lag-13.ch/job/MK-TFHE%20Coordinator/lastSuccessfulBuild/artifact/coordinator.docker.tar
Trustee:  https://jenkins.lag-13.ch/job/MK-TFHE%20Trustee/lastSuccessfulBuild/artifact/trustee.docker.tar
Bulletin Board: https://jenkins.lag-13.ch/job/MK-TFHE%20BulletinBoard/lastSuccessfulBuild/artifact/bulletinboard.docker.tar

## Usage
### Coordinator
The Coordinator has to be started first. This can be achieved by the following command:

```
java -jar .\coordinator-[VERSION].jar
```

It is essential that the lib folder is located in the same directory as the Jar file.

A vote can be created in the admin view "/admin". The default login is "admin" and the password is "a47S3x5de2hvsBraD8GXY8K8".

The vote on the coordinator has to be started before the trustees can connect to it.

### Trustee
After a coordinator is started and the vote is also started on the coordinator the trustees can be started as well.
The trustee(s) can be started over the compiled executable. The ports and hostanme can be specified by setting the corresponding environment variables.

```
export COORDINATOR_URI=https://coordinator.[hostname]/
export SELF_HOSTNAME=trustee.[hostname]
export SELF_PORT=42044
export VOTE_ID=1
./mk-tfhe-trustee-server
```

SELF_HOSTNAME and SELF_PORT are the listing hostname and port. Additionally, there are the EXTERNAL_HOSTNAME and EXTERNAL_PORT, which are the public hostname and port. The later hostname and port is used by the coordinator to connect to currently set up trustee.

### Bulletin Board

The bulletinboard(s) can be started after the trustee(s). The key-generation has not to be completed, but the trustees have to be known by the coordinator. Otherwise, starting a bulletinboard is identical as to start a trustee.

```
export COORDINATOR_URI=https://coordinator.[hostname]/
export SELF_HOSTNAME=bulletinboard.[hostname]
export SELF_PORT=43044
export VOTE_ID=1
./mk-tfhe-bulletinboard-server
```

### Docker
Docker images can be loaded with the following command:
```
docker load -i [File]
```

Then they can be started with the following commands:
```
docker run -d -p 8080:8080 coordinator
docker run -d -p 42043:42043 -e COORDINATOR_URI=[coordinator url] -e EXTERNAL_HOSTNAME=[hostname] -e EXTERNAL_PORT=[port] trustee
docker run -d -p 42143:42143 -e COORDINATOR_URI=[coordinator url] -e EXTERNAL_HOSTNAME=[hostname] -e EXTERNAL_PORT=[port] bulletinboard
```
